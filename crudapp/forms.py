from django import forms
from crudapp.models import Userdetail

class Userdetails_form(forms.ModelForm):
    class Meta:
        model = Userdetail
        fields = ('__all__')