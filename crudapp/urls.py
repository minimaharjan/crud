from django.contrib import admin
from django.urls import path
from crudapp import views
app_name = 'crud'
urlpatterns = [
    path('', views.read, name='read' ),
    path('add/', views.userdetail, name='add'),
    path('update/<int:id>/', views.update, name='update'),
    path('delete/<int:id>/', views.delete, name='delete'),
]