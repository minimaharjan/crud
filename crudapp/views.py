from django.shortcuts import render, redirect
from crudapp.forms import Userdetails_form
from .models import Userdetail
from django.http import HttpResponse

# Create your views here.
def userdetail(request):
    if request.method == "POST":
        form = Userdetails_form(request.POST, request.FILES)
        if form.is_valid():
            try:
                form.save()

                return redirect('crud:read')

            except:
                return HttpResponse('failure')

        else:
            return HttpResponse(form.errors)

    else:
        return render(request, 'index.html')

def read(request):
    users = Userdetail.objects.all()
    return render(request, 'views.html',{'users':users})


def update(request, id):
    users = Userdetail.objects.get(id=id)
    if request.method == "POST":
        form = Userdetails_form(request.POST, instance=users)
        if form.is_valid():
            try:
                form.save()
                return redirect('crud:read')
            except:
                return HttpResponse('failure')

        else:
            return HttpResponse(form.errors)    
    else:
        return render(request, 'update.html',{'users':users})

def delete(request, id):
    users = Userdetail.objects.get(id=id)
    users.delete()
    return redirect('crud:read')
 